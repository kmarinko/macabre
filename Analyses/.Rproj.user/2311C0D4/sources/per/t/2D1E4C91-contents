############################################################
### Analyses for study - KB ###
############################################################

library(dplyr)

############################################################
#### read file ####
############################################################

file="../data/exp3data_numeric.csv"
names=read.csv(file, nrows = 1); names=t(names)
df=read.csv(file); df = df[-c(1:3),]

#exclude cases who failed attn check/consent, tests, weren't allocated to a condition
df = df %>% filter(CNSNT==1, FOCUS1==2, FOCUS2==2, DistributionChannel!="preview", !is.na(Competition), Competition!="")

############################################################
#### Recode variables ####
############################################################

#recode - sex "Gender" M = 1, F = 2, NA = 3
summary(as.factor(df$Gender))
#quite a few people put gender as "3", recoding now to gather as much as we can.
summary(as.factor(df$Gender_3_TEXT))
df$sex = ifelse(df$Gender_3_TEXT=="MALE", "1", df$Gender)
df$sex = as.factor(ifelse(df$sex=="1", "M", ifelse(df$sex=="2", "F", NA))) #ignore lesbian/gay men, as study was only for mainly HET people
summary(df$sex)
#exclude cases with no sex (we don't know whether they were randomized into correct paradigm)
df = df %>% filter(!is.na(sex))

#recode - relstatus "RELASTATUS" single = 10; lt = 11; dating = 12 or 13
df$relstat = as.factor(ifelse(df$RELASTATUS==10, "single", ifelse(df$RELASTATUS>=12, "dating", "long-term relationship")))
summary(df$relstat)

#### Calculate indices #### 

#variable calculation - MV                  "MVI1.2_1"              "MVI1.2_2"              "MVI3"                  "MVI4"
#make all numeric
df = df %>% mutate_at(vars(starts_with("MVI")), ~as.numeric(.))
df = df %>% mutate(mv = rowMeans(.[grep(glob2rx("MVI*"), names(.))], na.rm = T))

#variable calculation - SOI
"BEHV1"                 "BEHV2"                 "BEHV3"                
"ATD_1"                 "ATD_2"                 "ATD_3" (R)
"DESIRE_1"              "DESIRE_2"              "DESIRE_3"             
"DESIRE_4"

#make all numeric
df = df %>% mutate_at(vars(c(starts_with("BEHV"), starts_with("ATD_"), starts_with("DESIRE_"))), ~as.numeric(.))

#these are not on the same scale, so rescaling for consistency
df$BEHV1 = scales::rescale(df$BEHV1, to =c(1,7))
df$BEHV2 = scales::rescale(df$BEHV2, to =c(1,7))
df$BEHV3 = scales::rescale(df$BEHV3, to =c(1,7))
#reverse code this one
df$ATD_3R = recode(df$ATD_3, `1` = 7, `2` = 6, `3` = 5, `4` = 4, `5` = 3, `6` = 2, `7` = 1)

df = df %>% rowwise() %>% mutate(
  soi = mean(c(BEHV1, BEHV2, BEHV3, ATD_1, ATD_2, ATD_3R, DESIRE_1, DESIRE_2, DESIRE_3, DESIRE_4), na.rm = T))
  
############################################################
#### Organise outcome variables for analysis #### 
############################################################

#### high quality individuals - MEN

Noah 
"Dating_HQ1"
"Desirability_HQ1"
"Male_profiles_2"
"Male_HCT_2" #(money invested in Noah after high competition treatment, where competition was high (i.e., interest based on quality))
"Male_LCT_2" #(money invested in Noah after high competition treatment, where competition was low (i.e., interest flat line) )

Liam 
"Dating_HQ2"
"Desirability_HQ2"
"Male_profiles_4"
"Male_HCT_4" #(money invested in Liam after high competition treatment, where competition was high (i.e., interest based on quality))
"Male_LCT_4" #(money invested in Liam after high competition treatment, where competition was low (i.e., interest flat line) )

#### low quality individuals - MEN

William 
"Dating_LQ1"
"Desirability_LQ1" 
"Male_profiles_1" 
"Male_HCT_1" #(money invested in William after high competition treatment, where competition was high (i.e., interest based on quality))
"Male_LCT_1" #(money invested in William after high competition treatment, where competition was low (i.e., interest flat line) )

Benjamin 
"Dating_LQ2"
"Desirability_LQ2"
"Male_profiles_3"
"Male_HCT_3" #(money invested in Benjamin after high competition treatment, where competition was high (i.e., interest based on quality))
"Male_LCT_3" #(money invested in Benjamin after high competition treatment, where competition was low (i.e., interest flat line) )

#### high quality individuals - WOMEN

Emma 
"Dating_HQ1f"
"Desirability_HQ1f"
"female_profiles_2"
"Female_HCT_2" #(money invested in Emma after high competition treatment, where competition was high (i.e., interest based on quality))
"Female_LCT_2" #(money invested in Emma after high competition treatment, where competition was low (i.e., interest flat line) )

Isabella 
"Dating_HQ2f"
"Desirability_HQ2f"
"female_profiles_4"
"Female_HCT_4" #(money invested in Isabella after high competition treatment, where competition was high (i.e., interest based on quality))
"Female_LCT_4" #(money invested in Isabella after high competition treatment, where competition was low (i.e., interest flat line) )


#### low quality individuals - WOMEN

Olivia 
"Dating_LQ1f"
"Desirability_LQ1f" 
"female_profiles_1" 
"Female_HCT_1" #(money invested in Olivia after high competition treatment, where competition was high (i.e., interest based on quality))
"Female_LCT_1" #(money invested in Olivia after high competition treatment, where competition was low (i.e., interest flat line) )

Ava 
"Dating_LQ2f"
"Desirability_LQ2f"
"female_profiles_3"
"Female_HCT_3" #(money invested in Ava after high competition treatment, where competition was high (i.e., interest based on quality))
"Female_LCT_3" #(money invested in Ava after high competition treatment, where competition was low (i.e., interest flat line) )


############################################################
#### Manipulation checks ####
############################################################

#Manipulation check

#make all numeric
df = df %>% mutate_at(vars(c(starts_with("Desirability_"), starts_with("Dating_"))), ~as.numeric(.))

df = df %>% rowwise() %>% mutate(
  desirability_LQ = mean(c(Desirability_LQ1, Desirability_LQ1f, Desirability_LQ2, Desirability_LQ2f), na.rm = T),
  desirability_HQ = mean(c(Desirability_HQ1, Desirability_HQ1f, Desirability_HQ2, Desirability_HQ2f), na.rm = T),
  dating_LQ = mean(c(Dating_LQ1, Dating_LQ1f, Dating_LQ2, Dating_LQ2f), na.rm = T),
  dating_HQ = mean(c(Dating_HQ1, Dating_HQ1f, Dating_HQ2, Dating_HQ2f), na.rm = T))

#anova to see difference
t.test(df$desirability_LQ, df$desirability_HQ)
t.test(df$dating_LQ, df$dating_HQ)

############################################################
#### Restructuring data for main hypothesis test ####
############################################################

#make all numeric
df = df %>% mutate_at(vars(c(contains("ale_HCT_"), contains("ale_LCT_"), contains("ale_profiles_"))), ~as.numeric(.))

#recode into four variables - investment in low and high quality individuals and time 1 and time 2

df = df %>% rowwise() %>% mutate(
  investmentT1_LQ = sum(c(female_profiles_1, female_profiles_3, Male_profiles_1, Male_profiles_3), na.rm = T), #time 1, investment in individuals 1 and 3
  investmentT2_LQ = sum(c(Female_HCT_1, Female_LCT_1, Female_HCT_3, Female_LCT_3, 
                           Male_HCT_1, Male_LCT_1, Male_HCT_3, Male_LCT_3), na.rm = T), #time 2, investment in 1 and 3 (ignoring competition treatment)
  investmentT1_HQ = sum(c(female_profiles_2, female_profiles_4, Male_profiles_2, Male_profiles_4), na.rm = T), #time 1, investment in individuals 2 and 4
  investmentT2_HQ = sum(c(Female_HCT_2, Female_LCT_2, Female_HCT_4, Female_LCT_4, 
                           Male_HCT_2, Male_LCT_2, Male_HCT_4, Male_LCT_4), na.rm = T)) #time 2, investment in 2 and 4 (ignoring competition treatment)

#competition treatment = "df$Competition"

#### hypothesis test - repeated anova - data must be in long format ###

#create id variable
df <- tibble::rowid_to_column(df, "id")
#subset df with relevant variables
df1 = df %>% select(id, Competition, contains("invest"), mv, soi)

#restructure to long 1 (time by competition)
df_long = reshape(as.data.frame(df1), direction='long', 
        varying=list(c('investmentT1_LQ', 'investmentT2_LQ'), c('investmentT1_HQ', 'investmentT2_HQ')), 
        timevar='time', idvar='id',
        times=c('T1', 'T2'),
        v.names=c('LQ', 'HQ')
        )
#restructure to long 2 (time by competition by quality)
df_long2 = reshape(as.data.frame(df_long), direction='long', 
                  varying=list(c('LQ', 'HQ')), 
                  timevar='quality', idvar=c('id', 'time'),
                  v.names=c('investment')
                  )
df_long2$quality = recode(df_long2$quality, `1` = "Low qual", `2` = "High qual")
df_long2$Competition = recode(df_long2$Competition, `Low` = "Low comp", `High` = "High comp")

############################################################
#### Running main hypothesis test ####
############################################################
#https://www.datanovia.com/en/lessons/repeated-measures-anova-in-r/#three-way
  
library(tidyverse)
library(ggpubr)
library(rstatix)

df_long2 %>% sample_n_by(Competition, time, quality, size = 1)

#### check out summary statistics ####
df_long2 %>% group_by(Competition, time, quality) %>% get_summary_stats(investment, type = "mean_sd")

#### check outliers - no extreme values ####
df_long2 %>% group_by(Competition, time, quality) %>% identify_outliers(investment)

#### check normality - fucked ####
norm = df_long2 %>% group_by(Competition, time, quality) %>% shapiro_test(investment)
ggqqplot(df_long2$investment)
ggdensity(df_long2$investment)

#### box plots ####
bxp <- ggboxplot(
  df_long2, x = "quality", y = "investment", color = "time", palette = "jco",
  facet.by = "Competition", short.panel.labs = FALSE)
bxp

#### run rmanova for three way interaction ####
rmanova.3way <- anova_test(data = df_long2, dv = investment, wid = id, within = c(quality, time), between = c(Competition))
rmanova.3way

#### split into simple two-way interactions at the between level factor ####
rmanova.2way <- df_long2 %>% group_by(Competition) %>% anova_test(dv = investment, wid = id, within = c(quality, time))
rmanova.2way

#### split into one way tests ####
rmanova.1way <- df_long2 %>% group_by(Competition, quality) %>% anova_test(dv = investment, wid = id, within = time)
rmanova.1way

#follow up to determine which groups are different
# Pairwise comparisons
pwc <- df_long2 %>%
  group_by(Competition, quality) %>%
  pairwise_t_test(investment ~ time, paired = TRUE, p.adjust.method = "bonferroni") %>%  select(-df, -statistic) # Remove details

#### SUMMARY ####
There is a three way interaction, only one slope is significant. 
In conditions of high competition, people lower their investment in low quality individuals.
People *almost* lowered their investment in low qual individuals in low competition, but p = .085.

# Visualization: box plots with p-values if we want something like this.
pwc <- pwc %>% add_xy_position(x = "Quality")
pwc.filtered <- pwc %>% filter(Competition == "High comp", quality == "Low qual")
bxp +  stat_pvalue_manual(pwc.filtered, tip.length = 0, hide.ns = TRUE) +
  labs(subtitle = get_test_label(rmanova.3way, detailed = TRUE),
    caption = get_pwc_label(pwc))

############################################################
#### Robustness Tests ####
############################################################

#### robustness run rmanova for three way interaction ####
rmanova.3way.robust <- anova_test(data = df_long2, dv = investment, wid = id, within = c(quality, time), between = c(Competition), covariate = c(mv, soi))
rmanova.3way.robust

#### robustness split into simple two-way interactions at the between level factor ####
rmanova.2way.robust <- df_long2 %>% group_by(Competition) %>% anova_test(dv = investment, wid = id, within = c(quality, time), covariate = c(mv, soi))
rmanova.2way.robust

#### robustnesssplit into one way tests ####
rmanova.1way.robust <- df_long2 %>% group_by(Competition, quality) %>% anova_test(dv = investment, wid = id, within = time, covariate = c(mv, soi))
rmanova.1way.robust

#### SUMMARY ####
The three way interaction - whereby in conditions of high competition, people lower their investment in low quality individuals - is moderated by mv.
There is also an soi*quality interaction.

#Haven't decomposed these yet.